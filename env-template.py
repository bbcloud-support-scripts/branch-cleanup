username      = '' # Cloud admin username
password      = '' # Cloud app password
workspace     = '' # Cloud workspace slug/id (as seen in the url)
repository    = '' # Repository slug (as seen in the url)
days          = 'x days' # The number of days a branch has not been updated with
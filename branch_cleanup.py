from requests import Session
from time import sleep
import pandas as pd
import datetime

try:
    import env
    username    = env.username
    password    = env.password
    workspace   = env.workspace
    repository  = env.repository
    days        = env.days

    url = f"https://api.bitbucket.org/2.0/repositories/{workspace}/{repository}/refs/branches"
    branches_found = []

    today = datetime.datetime.now()
    session = Session()
    session.auth = (username, password)
except [ImportError, AttributeError]:
    print('Could not locate one or more attributes within the "env.py" file. '
          'Please follow the readme to ensure that all attributes are present and try again.')
    exit()

def get_branches(page=None):
    while True:
        params = {'page': page, 'pagelen': 100, 'sort': '-target.date'}
        r = session.get(url, params=params)
        while r.status_code == 429:
            print("Hit the API rate limit. Sleeping for 10 sec...")
            sleep(10)
            print("Resuming...")
            r = session.get(url, params=params)
        r_data = r.json()
        for branch in r_data.get('values'):
            branch_name = branch.get('name').strip()
            last_updated = branch.get('target').get('date')[0:10].split('-')
            commits_url = branch.get('links').get('commits').get('href')
            yield branch_name, last_updated, commits_url 
        if not r_data.get('next'):
            return
        if page == None:
            page = 1
        page += 1

def get_last_commiter(commits_url):
    r = session.get(commits_url)
    r_json = r.json()
    commits_list = r_json.get('values')
    return commits_list[0].get('author').get('user').get('display_name')

def check_date(updated):
    insertion_date = datetime.datetime(int(updated[0]), int(updated[1]), int(updated[2]))
    if today - insertion_date > pd.Timedelta(days):
        return True
    else:
        return False

def delete_branch(branch):
    r = session.delete(url + '/' + branch)
    while r.status_code == 429:
        print("Hit the API rate limit. Sleeping for 10 sec...")
        sleep(10)
        print("Resuming...")
        r = session.delete(url + '/' + branch)
    return r.status_code, r.text

def main():
    print(f"The following branches have not been updated in the last {days}:")
    for branch, updated, commits in get_branches():
        if check_date(updated):
            branches_found.append(branch)
            last_commiter = get_last_commiter(commits)
            print(f"Branch Name:   {branch}")
            print(f"Last Updated:  {updated[0]}/{updated[1]}/{updated[2]}")
            print(f"Last Commiter: {last_commiter}\n")
    confirmation = input("Do you wish to delete them? [y/n]: ")
    if confirmation == "y":
        for branch in branches_found:
            s, m = delete_branch(branch)
            if s == 204:
                print(f"The branch {branch} was deleted!")
            else:
                print(f"Something went wrong with branch {branch}! - Status Code: {s} - {m}")
    else:
        print("Leaving without deleting branches.")

if __name__ == '__main__':
    main()